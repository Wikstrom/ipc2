import * as grammar from "./ipc2_grammar";
import * as print from "./ipc2_print";

const fs = require("fs");
const jison = require("jison");

const parser = new jison.Parser(grammar.ipc2_grammar);

/*
// Alternative 1
const parserSource = parser.generate();

fs.writeFile("ipc2parser.js", parserSource, function (err) {
    if (err) return console.log(err);
   });
*/

// Alternative 2
if (process.argv.length < 3) {
   console.log('Syntax: node ipc2.js <file.ipc2 / file.ipc2e>');
   process.exit(0);
}

function get_imported_files(tree : grammar.Theory) {
    let imported_files = [];
    for (const decl of tree.declarations) {
        if (decl.type == grammar.NodeType.Import) {
            const imp = decl as grammar.Import;

            imported_files.push(imp.path);
        }
    }

    return imported_files;
}

function dir_from_filename(filename) {
    if (filename.includes('/')) {
        return filename.substring(0, filename.lastIndexOf('/'));
    } else {
        return ".";
    }
}

function simplify_filename(filename)
{
    const filename_parts_input = filename.split('/');

    let filename_part_to_use = [];

    for (const part of filename_parts_input) {
        if (part == ".") {
            // Do nothing
        } else if (part == "..") {
            if (filename_part_to_use.length == 0) {
                filename_part_to_use.push("..");
            } else if (filename_part_to_use[filename_part_to_use.length - 1] == "..") {
                filename_part_to_use.push("..");
            } else {
                filename_part_to_use.pop();
            }
        } else {
            filename_part_to_use.push(part);
        }
    }

    let res = "";

    for (let p of filename_part_to_use) {
        if (res != "") {
            res += "/";
        }

        res += p;
    }

    return res;
}

async function read_ipc2_file(filename, files_read_in) {
    let files_read = Array.from(files_read_in);

    if (filename.startsWith("/")) {
        console.error("An import directive must not specify an absolute path: " + filename);
        process.exit();
    }

    filename = simplify_filename(filename);
    
    let parse_tree = await read_ipc2_file_internal(filename) as any as grammar.Theory;

    files_read[filename] = parse_tree;

    const imported_files = get_imported_files(parse_tree);

    const dir = dir_from_filename(filename);
    for (const imported_file of imported_files) {

        if (!(imported_file in files_read)) {
            let files_read_new = await read_ipc2_file(dir + "/" + imported_file, files_read);

            for (let idx in files_read_new) {
                files_read[idx] = files_read_new[idx];
            }
        }
    }

    return files_read;
}

async function read_ipc2_file_internal(filename : string) { 
    if (filename.endsWith(".ipc2") || filename.endsWith(".ipc2e")) {
        console.error("An import directive must not specify the file extension: " + filename);
        process.exit();
    }
    
    let fileWithIpc2ExtensionExists = false;
    let fileWithIpc2eExtensionExists = false;

    if (fs.existsSync(filename + ".ipc2")) {
        fileWithIpc2ExtensionExists = true;
    }
    
    if (fs.existsSync(filename + ".ipc2e")) {
        fileWithIpc2eExtensionExists = true;
    }

    if (fileWithIpc2ExtensionExists) {
        filename += ".ipc2";
    }

    if (fileWithIpc2eExtensionExists) {
        filename += ".ipc2e";
    }

    return new Promise<string>((resolve, reject) => {
        fs.readFile(filename, "utf8", function (err, data) {
            if (err) throw err;
          
            const parse_tree = parser.parse(data); 
    
            resolve(parse_tree);
        });
    });
}

function combine_theories(theories : [grammar.Theory]) : grammar.Theory {
    let declarations = [];

    for (let name in theories) {
        for (const decl of theories[name].declarations) {
            if (decl.type != grammar.NodeType.Import) {
                declarations.push(decl);
            }
        }
    }

    return {type: 'theory', declarations: declarations} as grammar.Theory;
}

async function main() {
    let data = await read_ipc2_file(process.argv[2], []);

    console.log(print.tree_to_string(combine_theories(data as [grammar.Theory]), 0));

    /*for (let filename in data) {
        console.log("[" + filename + "]");
        console.log(tree_to_string((data as any)[filename]));
    }*/
}

main();
