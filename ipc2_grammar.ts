export const ipc2_grammar = {
    "lex": {
        "rules": [
            ["\\s+", "/* skip whitespace */"],            
            ["\\.\\.", "return 'dotdot';"],
            ["\\.", "return 'dot';"],
            ["[(]", "return 'left_par';"],
            ["[)]", "return 'right_par';"],
            ["[{]", "return 'left_curly';"],
            ["[}]", "return 'right_curly';"],
            ["\\[", "return 'left_bracket';"],
            ["\\]", "return 'right_bracket';"],
            ["[&][&]", "return 'and';"],
            ["[|][|]", "return 'or';"],
            ["/", "return 'slash';"],
            ["==", "return 'eq';"],
            [":=", "return 'eq_def';"],
            ["-->", "return 'implies';"],
            ["<--", "return 'part_of';"],
            [",", "return 'comma';"],
            [";", "return 'semicolon';"],
            ["::", "return 'double_colon';"],
            ["\\\"[^\\\"]*\\\"", "return 'string';"],
            ["\\<", "return 'left_angle_bracket';"],
            ["\\>", "return 'right_angle_bracket';"],
            ["[1-9][0-9]*", "return 'INTEGER';"],
            ["const\\s+", "return 'const';"],
            ["truth\\s+", "return 'truth';"],
            ["def\\s+", "return 'def';"],
            ["import\\s+", "return 'import';"],
            ["forall\\s+", "return 'forall';"],
            ["exists\\s+", "return 'exists';"],
            ["namespace\\s+", "return 'namespace';"],
            ["in\\s+", "return 'in';"],
            ["[a-z][a-zA-Z0-9_]*", "return 'CONST_IDENT';"],
            ["[A-Z][a-zA-Z0-9_]*", "return 'PRED_IDENT';"],

        ]
    },
    "bnf": {
        "ipc2_theory": [
            ["declarations", "return {type: 'theory', declarations: $$};"]
        ] ,
        "declarations": [
            ["declarations declaration", "$$ = $1.concat([$2]);"], 
            ["declaration", "$$ = [$1];"]
        ],
        "declaration": [
            ["import_decl", "$$ = $1;"], 
            ["const_decl", "$$ = $1;"],
            ["truth_decl", "$$ = $1;"],
            ["def_decl", "$$ = $1;"],
            ["namespace_decl", "$$ = $1;"],
        ],
        "import_decl": [
            ["import string semicolon", "$$ = {type: 'import', path: $2.slice(1, -1)}"]
        ],
        "namespace_decl": [
            ["namespace const_ident left_curly declarations right_curly", "$$ = {type: 'namespace', name: $2, contents: $4}"]
        ],
        "ident": ["CONST_IDENT", "PRED_IDENT"],
        "const_decl": [
            ["const pred_or_const_idents semicolon", "$$ = {type: 'const', constants: $2}"]
        ],
        "pred_or_const_idents": [
            ["pred_or_const_idents comma const_or_pred_with_arity", "$$ = $1.concat([$3])"],
            ["const_or_pred_with_arity", "$$ = [$1]"]
        ],
        "const_idents": [
            ["const_idents comma const_ident", "$$ = $1.concat([$3])"],
            ["const_ident", "$$ = [$1]"]
        ],
        "const_or_pred_with_arity": [
            ["pred_with_arity", "$$ = $1"],
            ["const_ident", "$$ = $1"]
        ],
        "pred_with_arity": [
            ["PRED_IDENT slash INTEGER", "$$ = {type: 'constant', constant: $1, arity: $3}"],
        ],
        "pred_ident": [
            ["PRED_IDENT", "$$ = {type: 'constant', constant: $1}"],
        ],
        "const_ident": [
            ["CONST_IDENT", "$$ = {type: 'constant', constant: $1}"]   
        ],
        "pred_ref": [
            ["pred_ident", "$$ = {type: 'reference', constant: $1}"],
            ["const_ident double_colon pred_ref", "$$ = {type: 'reference', constant: {type: 'constant', constant: $1.constant + '::' + $3.constant.constant}}"],            
        ],
        "const_ref": [
            ["const_ident", "$$ = {type: 'reference', constant: $1}"],
            ["const_ident double_colon const_ref", "$$ = {type: 'reference', constant: {type: 'constant', constant: $1.constant + '::' + $3.constant.constant}}"],
        ],
        "def_decl": [
                ["def PRED_IDENT left_par const_idents right_par eq_def expr semicolon",
                    "$$ = {type: 'def', pred: $2, constants: $4, expression: $7}"]
                    ,
                ["def left_angle_bracket const_ident right_angle_bracket const_ident dot const_ident eq_def expr semicolon", 
                    "$$ = {type: 'qdef', quantifier: $3, variable: $5, expression1: $7, expression2: $9}"]
            ],
        "truth_decl": [
            ["truth expr semicolon", "$$ = {type: 'truth', expression: $2}"]
        ],
        "primary_expr": [
            ["left_par expr right_par", "$$ = $2"], 
            ["left_par forall const_ident in expr dotdot expr dot expr right_par", "$$ = {type: 'forall', var: $3, min: $5, max: $7, expr: $9}"],
            ["left_par exists const_ident in expr dotdot expr dot expr right_par", "$$ = {type: 'exists', var: $3, min: $5, max: $7, expr: $9}"], 
            ["left_par left_angle_bracket const_ident right_angle_bracket const_ident dot expr right_par", "$$ = {type: 'quantifier', quantifier: $3, var: $5, expr: $7}"],
            ["pred_ref left_par exprs right_par", "$$ = {type: 'appl', pred: $1, operands: $3}"],
            "const_ref"
        ],
        "and_or_expr": [
            "primary_expr", 
            ["primary_expr and and_or_expr", "$$ = {type: 'and', left: $1, right: $3}"], 
            ["primary_expr or and_or_expr", "$$ = {type: 'or', left: $1, right: $3}"]
        ],
        "implies_indexed": [
            ["implies left_bracket expr right_bracket", "$$ = $3"]
        ],
        "part_of_indexed": [
            ["part_of left_bracket expr right_bracket", "$$ = $3"]
        ],
        "eq_indexed": [
            ["eq left_bracket expr right_bracket", "$$ = $3"]
        ],
        "impl_eq_expr": [
            "and_or_expr", 
            ["impl_eq_expr implies_indexed and_or_expr", "$$ = {type: 'implies_', left: $1, index: $2, right: $3}"], 
            ["impl_eq_expr part_of_indexed and_or_expr", "$$ = {type: 'part_of_', left: $1, index: $2, right: $3}"], 
            ["impl_eq_expr eq_indexed and_or_expr", "$$ = {type: 'eq_', left: $1, index: $2, right: $3}"], 
            ["impl_eq_expr implies and_or_expr", "$$ = {type: 'implies', left: $1, right: $3}"], 
            ["impl_eq_expr part_of and_or_expr", "$$ = {type: 'part_of', left: $1, right: $3}"], 
            ["impl_eq_expr eq and_or_expr", "$$ = {type: 'eq', left: $1, right: $3}"],
        ],
        "expr": [["impl_eq_expr", "$$ = $1"]],
        "exprs": [
            ["exprs comma expr", "$$ = $1.concat($3)"],
            ["expr", "$$ = [$1]"]
        ],
    }
};

export enum NodeType {
    Theory = 'theory',
    Import = 'import', 
    Const = 'const',
    Truth = 'truth',
    Def = 'def',
    QDef = 'qdef',
    Namespace = 'namespace',
    Reference = 'reference',
    Constant = 'constant',
    Appl = 'appl',
    ImpliesIndexed = 'implies_',
    PartOfIndexed = 'part_of_',
    EqIndexed = 'eq_',
    Eq = 'eq',
    Implies = 'implies',
    PartOf = 'part_of',
    And = 'and',
    Or = 'or',
    Forall = 'forall',
    Exists = 'exists',
    Quantifier = 'quantifier',
}

export interface Tree {
    readonly type: NodeType;
}

export interface Theory {
    readonly type: NodeType.Theory;
    readonly declarations: [Tree];
}

export interface Import {
    readonly type: NodeType.Import;
    readonly path: string;
}

export interface Const {
    readonly type: NodeType.Const;
    readonly constants: [Tree];
}

export interface Truth {
    readonly type: NodeType.Truth;
    readonly expression: Tree;
}

export interface Def {
    readonly type: NodeType.Def;
    readonly constants: [Tree];
    readonly pred: string;
    readonly expression: Tree;
}

export interface QDef {
    readonly type: NodeType.QDef;
    readonly quantifier: Tree;
    readonly variable: Tree;
    readonly expression1: Tree;
    readonly expression2: Tree;
}

export interface Namespace {
    readonly type: NodeType.Namespace;
    readonly name: Tree;
    readonly contents: [Tree];
}

export interface Reference {
    readonly type: NodeType.Namespace;
    readonly constant: Tree;
}

export interface Constant {
    readonly type: NodeType.Constant;
    readonly constant: string;
    readonly arity: string;
}

export interface Appl {
    readonly type: NodeType.Appl;
    readonly pred: Tree;
    readonly operands: [Tree];
}

export interface ImpliesIndexed {
    readonly type: NodeType.ImpliesIndexed;
    readonly left: Tree;
    readonly index: Tree;
    readonly right: Tree;
}

export interface PartOfIndexed {
    readonly type: NodeType.PartOfIndexed;
    readonly left: Tree;
    readonly index: Tree;
    readonly right: Tree;
}

export interface EqIndexed {
    readonly type: NodeType.EqIndexed;
    readonly left: Tree;
    readonly index: Tree;
    readonly right: Tree;
}

export interface Eq {
    readonly type: NodeType.Eq;
    readonly left: Tree;
    readonly right: Tree;
}

export interface Implies {
    readonly type: NodeType.Implies;
    readonly left: Tree;
    readonly right: Tree;
}

export interface PartOf {
    readonly type: NodeType.PartOf;
    readonly left: Tree;
    readonly right: Tree;
}

export interface And {
    readonly type: NodeType.And;
    readonly left: Tree;
    readonly right: Tree;
}

export interface Or {
    readonly type: NodeType.Or;
    readonly left: Tree;
    readonly right: Tree;
}

export interface Forall {
    readonly type: NodeType.Forall;
    readonly var: Tree;
    readonly min: Tree;
    readonly max: Tree;
    readonly expr: Tree;
}

export interface Exists {
    readonly type: NodeType.Exists;
    readonly var: Tree;
    readonly min: Tree;
    readonly max: Tree;
    readonly expr: Tree;
}

export interface Quantifier {
    readonly type: NodeType.Quantifier;
    readonly quantifier: Tree;
    readonly var: Tree;
    readonly expr: Tree;
}