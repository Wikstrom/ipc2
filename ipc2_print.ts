import * as grammar from "./ipc2_grammar";

const fs = require("fs");
const jison = require("jison");

function createSpaces(n : Number) {
    let res = "";

    for (let i = 0; i < n; i++) {
        res += " ";
    }

    return res;
}

const spacesPerTab = 2;

export let tree_to_string = (tree : grammar.Tree, indentation) : string => {
    switch (tree.type) {
        case grammar.NodeType.Theory:
            return (tree as grammar.Theory).declarations.reduce((accumulator, currentValue) => accumulator + createSpaces(indentation) + tree_to_string(currentValue, indentation) + ";\n", "");
        case grammar.NodeType.Import:
            return "import " + (tree as grammar.Import).path;
        case grammar.NodeType.Const:
            return "const " + (tree as grammar.Const).constants.reduce((accumulator, currentValue) => 
                (accumulator == "" ? "": accumulator + ", ") + tree_to_string(currentValue, indentation), "");        
        case grammar.NodeType.Constant:
            return (tree as grammar.Constant).constant + ((tree as grammar.Constant).arity ? "/" + (tree as grammar.Constant).arity : "");
        case grammar.NodeType.Truth:
            return "truth " + tree_to_string((tree as grammar.Truth).expression, indentation);
        case grammar.NodeType.Def:
            return "def " + (tree as grammar.Def).pred 
            + "(" 
            + (tree as grammar.Def).constants.reduce((accumulator, currentValue) => 
                (accumulator == "" ? "": accumulator + ", ") + tree_to_string(currentValue, indentation), "")
            + ") := "
            + tree_to_string((tree as grammar.Def).expression, indentation);
        case grammar.NodeType.QDef:
            return "qdef <" + tree_to_string((tree as grammar.QDef).quantifier, indentation) + ">"
                + " " + tree_to_string((tree as grammar.QDef).variable, indentation)
                + " " + tree_to_string((tree as grammar.QDef).expression1, indentation)
                + " " + tree_to_string((tree as grammar.QDef).expression2, indentation)
                ;     
        case grammar.NodeType.Namespace:
            {
                let str = "";
                
                for (let decl of (tree as grammar.Namespace).contents) {
                    str += createSpaces(indentation + spacesPerTab) + tree_to_string(decl, indentation + spacesPerTab) + ";\n";
                }

                return "namespace " + tree_to_string((tree as grammar.Namespace).name, indentation) + " {\n" + str + "}";
            }
        case grammar.NodeType.Reference:
            return tree_to_string((tree as grammar.Reference).constant, indentation);
        case grammar.NodeType.Appl:
            return tree_to_string((tree as grammar.Appl).pred, indentation) + "(" + (tree as grammar.Appl).operands.reduce((accumulator, currentValue) => 
            (accumulator == "" ? "": accumulator + ", ") + 
            tree_to_string(currentValue, indentation), "") + ")";
        case grammar.NodeType.ImpliesIndexed:
            return "(" + tree_to_string((tree as grammar.ImpliesIndexed).left, indentation) + " -->[" + tree_to_string((tree as grammar.ImpliesIndexed).index, indentation) + "] " + tree_to_string((tree as grammar.ImpliesIndexed).right, indentation) + ")";
        case grammar.NodeType.PartOfIndexed:
            return "(" + tree_to_string((tree as grammar.PartOfIndexed).left, indentation) + " <--[" + tree_to_string((tree as grammar.PartOfIndexed).index, indentation) + "] " + tree_to_string((tree as grammar.PartOfIndexed).right, indentation) + ")";
        case grammar.NodeType.EqIndexed:
            return "(" + tree_to_string((tree as grammar.EqIndexed).left, indentation) + " ==[" + tree_to_string((tree as grammar.EqIndexed).index, indentation) + "] " + tree_to_string((tree as grammar.EqIndexed).right, indentation) + ")";
        case grammar.NodeType.Eq:
            return "(" + tree_to_string((tree as grammar.Eq).left, indentation) + " == " + tree_to_string((tree as grammar.Eq).right, indentation) + ")";            
        case grammar.NodeType.Implies:
            return "(" + tree_to_string((tree as grammar.Implies).left, indentation) + " --> " + tree_to_string((tree as grammar.Implies).right, indentation) + ")";
        case grammar.NodeType.PartOf:
            return "(" + tree_to_string((tree as grammar.PartOf).left, indentation) + " <-- " + tree_to_string((tree as grammar.PartOf).right, indentation) + ")";
        case grammar.NodeType.And:
            return "(" + tree_to_string((tree as grammar.And).left, indentation) + " && " + tree_to_string((tree as grammar.And).right, indentation) + ")";
        case grammar.NodeType.Or:
            return "(" + tree_to_string((tree as grammar.Or).left, indentation) + " || " + tree_to_string((tree as grammar.Or).right, indentation) + ")";
        case grammar.NodeType.Forall:
            return "(forall " + tree_to_string((tree as grammar.Forall).var, indentation) + " in " + tree_to_string((tree as grammar.Forall).min, indentation) + " .. " + tree_to_string((tree as grammar.Forall).max, indentation) + "." + tree_to_string((tree as grammar.Forall).expr, indentation) + ")";
        case grammar.NodeType.Exists:
            return "(exists " + tree_to_string((tree as grammar.Exists).var, indentation) + " in " + tree_to_string((tree as grammar.Exists).min, indentation) + " .. " + tree_to_string((tree as grammar.Exists).max, indentation) + "." + tree_to_string((tree as grammar.Exists).expr, indentation) + ")";
        case grammar.NodeType.Quantifier:
            return "(<" + tree_to_string((tree as grammar.Quantifier).quantifier, indentation) + ">" + tree_to_string((tree as grammar.Quantifier).var, indentation) + "." + tree_to_string((tree as grammar.Quantifier).expr, indentation) + ")";
    }
}
